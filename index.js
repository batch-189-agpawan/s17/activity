/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
function enterName(){
	let userName = prompt('Enter your Full Name')
	console.log('Hello, ' + userName)
};

enterName();

function enterAge(){
	let userAge = prompt('Enter your Age')
	console.log('you are ' + userAge + ' years old')
};

enterAge()

function enterlocation(){
	let userlocation = prompt('Enter your location')
	console.log('you live in ' + userlocation)
};

enterlocation();
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
function displayAlbums(){
	console.log('1. Janno Gibbs')
	console.log('2. VST & Co.')
	console.log('3. Itchyworms')
	console.log('4. Apo Hiking Society')
	console.log('5. Asin')

};

displayAlbums();

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
function displayMovies(){
	console.log('1. THE LAWNMOWER MAN')
	console.log('Rotten Tomatoes Rating: 35%')

	console.log('2. HUSTLE')
	console.log('Rotten Tomatoes Rating: 92%')

	console.log('3. THE CARD COUNTER')
	console.log('Rotten Tomatoes Rating: 87%')

	console.log('4. INTERCEPTOR')
	console.log('Rotten Tomatoes Rating: 43%')

	console.log('5. JURASSIC PARK')
	console.log('Rotten Tomatoes Rating: 92%')

};

displayMovies();
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = alert("Enter your first friend's name:"); 
	let friend2 = prom("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friends); 
};


// console.log(friend1);
// console.log(friend2);